import sqlite3, datetime
import mysql
import mysql.connector

db = "wetter.db"

#For MySQL
#host = "127.0.0.1"
#user = "root"
#passwd = ""
#db = "wetter"

def connect_db():
    return sqlite3.connect(db)

    #MySQL Variante
    #return mysql.connector.connect(host=host, user=user, passwd=passwd, db=db)


def create_db():
    #Anlegen der Datenbank - wird nur beim ersten Mal benötigt
    conn = connect_db()
    cursor = conn.cursor()
    cursor.execute('CREATE TABLE wetter (station TEXT, date TEXT, temperature NUMERIC)')
    conn.close


def insert_db(station, temperature):
    # Übergabe der Werte an die Datenbank
    conn = connect_db()
    cursor = conn.cursor()
    date = datetime.datetime.now()
    cursor.execute("INSERT INTO wetter(station ,date , temperature) VALUES (?,?,?)",(station,date,temperature))
    conn.commit()
    conn.close

def clear_db():
    conn = connect_db()
    cursor = conn.cursor()
    cursor.execute("DELETE FROM wetter")
    conn.commit()
    conn.close


def read_db():
    conn = connect_db()
    cursor = conn.cursor()
    cursor.execute("SELEcT * FROM wetter")
    rows = cursor.fetchall()
    print(rows)
    conn.close


# Start des Web-Dienstes 
if __name__=='__main__':
    #create_db()
    read_db()